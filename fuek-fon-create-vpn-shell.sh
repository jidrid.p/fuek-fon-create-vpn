# 1. install open vpn
apt-get update
apt-get install openvpn easy-rsa
apt-get install wget
apt-get install ufw
apt install curl

# 2. Set Up the CA Directory
make-cadir ~/openvpn-ca
cd ~/openvpn-ca

# 3. Configure the CA Variables
cat ~/openvpn-ca/vars
sed -i 's/export KEY_COUNTRY="US"/export KEY_COUNTRY="TH"/g' ~/openvpn-ca/vars
sed -i 's/export KEY_PROVINCE="CA"/export KEY_PROVINCE="Bangkok"/g' ~/openvpn-ca/vars
sed -i 's/export KEY_CITY="SanFrancisco"/export KEY_CITY="Bangkok"/g' ~/openvpn-ca/vars
sed -i 's/export KEY_ORG="Fort-Funston"/export KEY_ORG="Fort-Funston"/g' ~/openvpn-ca/vars
sed -i 's/export KEY_EMAIL="me@myhost.mydomain"/export KEY_EMAIL="jidrid.p@gmail.com"/g' ~/openvpn-ca/vars
sed -i 's/export KEY_OU="MyOrganizationalUnit"/export KEY_OU="FuekFon"/g' ~/openvpn-ca/vars
sed -i 's/export KEY_NAME="EasyRSA"/export KEY_NAME="server"/g' ~/openvpn-ca/vars

# 4. Build the Certificate Authority
source vars
./clean-all
ln -s openssl-1.0.0.cnf openssl.cnf
./build-ca

# 5. Create the Server Certificate, Key, and Encryption Files
./build-key-server server
./build-dh
openvpn --genkey --secret keys/ta.key

# 6. Generate a Client Certificate and Key Pair
source vars
# ./build-key fuek-fon-admin not password
./build-key-pass fuek-fon-admin

# 7. Configure the OpenVPN Service
cd ~/openvpn-ca/keys
cp ca.crt server.crt server.key ta.key dh2048.pem /etc/openvpn
# gunzip -c /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz | sudo tee /etc/openvpn/server.conf
# gunzip -c /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz | tee /etc/openvpn/server.conf not have file
wget -O /etc/openvpn/server.conf https://raw.githubusercontent.com/OpenVPN/openvpn/master/sample/sample-config-files/server.conf
cat /etc/openvpn/server.conf
sed -i 's/cipher AES-256-CBC/cipher AES-128-CBC/g' /etc/openvpn/server.conf 
echo "\nauth SHA256" >> /etc/openvpn/server.conf 
sed -i 's/;user nobody/user nobody/g' /etc/openvpn/server.conf 
sed -i 's/;group nobody/group nogroup/g' /etc/openvpn/server.conf 
push "redirect-gateway def1 bypass-dhcp"
sed -i 's/;push "dhcp-option DNS 208.67.222.222"/push "dhcp-option DNS 208.67.222.222"/g' /etc/openvpn/server.conf 
sed -i 's/;push "dhcp-option DNS 208.67.220.220"/push "dhcp-option DNS 208.67.220.220"/g' /etc/openvpn/server.conf 
sed -i 's/port 1194/port 443/g' /etc/openvpn/server.conf 
sed -i 's/proto udp/proto tcp/g' /etc/openvpn/server.conf 

# 8. Adjust the Server Networking Configuration
cat /etc/sysctl.conf
sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
ip route | grep default
cat /etc/ufw/before.rules
# copy file local before.rules to /etc/ufw/before.rules
# ufw allow 1194/udp
# ufw allow OpenSSH

# 9. Start and Enable the OpenVPN Service
# systemctl start openvpn@server
# systemctl status openvpn@server
# ip addr show tun0
# systemctl enable openvpn@server

# 10. Create Client Configuration Infrastructure
mkdir -p ~/client-configs/files
chmod 700 ~/client-configs/files
wget -O ~/client-configs/base.conf https://raw.githubusercontent.com/OpenVPN/openvpn/master/sample/sample-config-files/client.conf
sed -i 's/;user nobody/user nobody/g' ~/client-configs/base.conf
sed -i 's/;group nobody/group nogroup/g' ~/client-configs/base.conf
# sed -i 's/remote my-server-1 1194/remote IP_SERVER 1194/g' ~/client-configs/base.conf
sed -i 's/cipher AES-256-CBC/cipher AES-128-CBC/g' ~/client-configs/base.conf
echo "\nauth SHA256" >> /etc/openvpn/server.conf 
echo "\nkey-direction 1" >> /etc/openvpn/server.conf 
wget -O ~/client-configs/make_config.sh https://gitlab.com/jidrid.p/fuek-fon-create-vpn/-/raw/master/make_config.sh.sh 
chmod 700 ~/client-configs/make_config.sh

# 11. Generate Client Configurations
cd ~/client-configs
./make_config.sh fuek-fon-admin
